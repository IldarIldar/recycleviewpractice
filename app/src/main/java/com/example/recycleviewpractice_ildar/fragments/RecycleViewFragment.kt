package com.example.recycleviewpractice_ildar.fragments
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener
import com.example.recycleviewpractice_ildar.SwipeToDelete
import com.example.recycleviewpractice_ildar.adapters.JokeAdapter
import com.example.recycleviewpractice_ildar.databinding.FragmentRecycleViewBinding
import com.example.recycleviewpractice_ildar.database.models.Joke
import com.example.recycleviewpractice_ildar.viewModel.ViewModel



class RecycleViewFragment : Fragment(), OnClickListener {

    private lateinit var jokeAdapter: JokeAdapter;
    private lateinit var binding: FragmentRecycleViewBinding
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    val viewModel: ViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding= FragmentRecycleViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val newJoke=binding.addStudentButton

        setUpRecycleView(this)

        val itemTouchHelper= ItemTouchHelper(SwipeToDelete(JokeAdapter(viewModel.getAllJokes() ,this, viewModel)))

        itemTouchHelper.attachToRecyclerView(binding.recyclerView)

        viewModel.jokesModel.observe(viewLifecycleOwner, Observer { currentJoke->
            println("\nDETECTO CAMBIOS\n")
            setUpRecycleView(this)
        })


        newJoke.setOnClickListener{
            viewModel.searchJoke()
        }



        //pasasr a otros fragments
        binding.toFavFragButton.setOnClickListener{
            val action = RecycleViewFragmentDirections.actionRecycleViewFragmentToFavouriteFragment()
            findNavController().navigate(action)
        }

        binding.searchButton.setOnClickListener{
            val action=RecycleViewFragmentDirections.actionRecycleViewFragmentToSearchFragment()
            findNavController().navigate(action)
        }


    }

    fun setUpRecycleView(lis:OnClickListener){
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter= JokeAdapter( viewModel.getAllJokes() ,lis, viewModel)
        }

    }


    override fun onClick(joke: Joke) {
        val action = RecycleViewFragmentDirections.actionRecycleViewFragmentToDetailFragment(joke) //pasamos añ siguiente fragment con un dato student
        findNavController().navigate(action)

    }


}