package com.example.recycleviewpractice_ildar.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener
import com.example.recycleviewpractice_ildar.SwipeToDelete
import com.example.recycleviewpractice_ildar.adapters.JokeAdapter
import com.example.recycleviewpractice_ildar.database.models.Joke
import com.example.recycleviewpractice_ildar.databinding.FragmentSearchBinding
import com.example.recycleviewpractice_ildar.viewModel.ViewModel


class SearchFragment : Fragment(), OnClickListener {
    private lateinit var jokeAdapter: JokeAdapter;
    private lateinit var binding: FragmentSearchBinding
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    val viewModel: ViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentSearchBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val itemTouchHelper= ItemTouchHelper(SwipeToDelete(JokeAdapter(viewModel.getAllJokes() ,this, viewModel)))
        itemTouchHelper.attachToRecyclerView(binding.recyclerSearchView)
        val lis=this
        val textEdit=binding.searchJokeText.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                setUpRecycleView(lis,p0)

            }

            override fun afterTextChanged(p0: Editable?) {}
        })


//        viewModel.jokesModel.observe(viewLifecycleOwner, Observer { currentJoke->
//            println("\nDETECTO CAMBIOS\n")
//            setUpRecycleView(this, p0)
//        })




    }

    fun setUpRecycleView(lis: SearchFragment, p0: CharSequence?){
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerSearchView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter= JokeAdapter( viewModel.searchJoke(p0.toString()) ,lis, viewModel)
        }

    }


    override fun onClick(joke: Joke) {
        val action = SearchFragmentDirections.actionSearchFragmentToDetailFragment(joke) //pasamos añ siguiente fragment con un dato student
        findNavController().navigate(action)

    }

}