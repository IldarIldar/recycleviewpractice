package com.example.recycleviewpractice_ildar.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.recycleviewpractice_ildar.databinding.FragmentDetailBinding
import com.example.recycleviewpractice_ildar.database.models.Joke
import com.example.recycleviewpractice_ildar.viewModel.ViewModel


class Detail_Fragment : Fragment() {

    lateinit var binding: FragmentDetailBinding
    val viewModel: ViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println("dentro de detail fragment")
        var joke=arguments?.get("joke") as Joke //recibimos los datos del fragment anterior y casteamos de any a student

        binding.nameView.setText("Joke Num "+joke.id)
        binding.nameContent.setText(joke.joke)
        //cambia el recycle layout view el nombre


        //change joke with chekbox
        binding.favorite.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            var fav=binding.favorite.isChecked
            viewModel.favJoke(joke,fav)
        })
        //change checkbox with joke
        binding.favorite.isChecked = joke.fav


    }

}