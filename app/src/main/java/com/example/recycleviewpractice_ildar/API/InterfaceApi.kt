package com.example.recycleviewpractice_ildar.API

import com.example.recycleviewpractice_ildar.database.models.Joke
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

interface InterfaceApi {
    //https://v2.jokeapi.dev/joke/Any
    //https://v2.jokeapi.dev/joke/Programming?type=single

    @GET()
    suspend fun getData(@Url url: String): Response<Joke>

    companion object {
        val BASE_URL = "https://v2.jokeapi.dev" //quizas deberia acabar en un /

        fun createRetrofit(): InterfaceApi {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(InterfaceApi::class.java)
        }
    }



}