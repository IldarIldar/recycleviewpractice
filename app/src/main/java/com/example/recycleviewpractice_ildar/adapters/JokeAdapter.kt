package com.example.recycleviewpractice_ildar.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewpractice_ildar.OnClickListener
import com.example.recycleviewpractice_ildar.R
import com.example.recycleviewpractice_ildar.databinding.ItemStudentBinding
import com.example.recycleviewpractice_ildar.database.models.Joke
import com.example.recycleviewpractice_ildar.viewModel.ViewModel

class JokeAdapter(
    private val jokes: List<Joke>,
    private val listener: OnClickListener,
    private val viewModel: ViewModel
)
    :RecyclerView.Adapter<JokeAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemStudentBinding.bind(view)

        fun setListener(joke: Joke) {
            binding.root.setOnClickListener {
                listener.onClick(joke)
            }
        }

    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_student, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val joke = jokes[position]
        with(holder){
            setListener(joke)
            binding.name.text =("Joke "+joke.id)


        }
    }

    override fun getItemCount(): Int {
        return jokes.size
    }

    fun deleteItem(pos: Int) {
        println("DELETE ITEM FUNCTION")
        val id = jokes[pos].id
        viewModel.deleteJoke(id)
    }

    }