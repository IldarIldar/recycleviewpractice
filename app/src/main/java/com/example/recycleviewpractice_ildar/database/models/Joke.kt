package com.example.recycleviewpractice_ildar.database.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Entity(tableName = "Joke")
@Parcelize
data class Joke(@SerializedName("joke") var joke:String,
                @PrimaryKey @SerializedName("id") var id: Int,
                var fav:Boolean=false): Parcelable

/**
https://sv443.net/jokeapi/v2/
https://v2.jokeapi.dev/joke/Programming?type=single

Joke(@SerializedName("error") var error:Boolean,@SerializedName("category") var category:String,
@SerializedName("type") var type: String,  @SerializedName("joke") var joke:String,
@SerializedName("flags") var flags: MutableMap<String,Boolean>,  @SerializedName("id") var id: Int,
@SerializedName("safe") var safe:Boolean, @SerializedName("lang")var lang: String): Parcelable

https://sv443.net/jokeapi/v2/#categories
{
"error": false,
"category": "Programming",
"type": "single",
"joke": "\"Honey, go to the store and buy some eggs.\"\n\"OK.\"\n\"Oh and while you're there, get some milk.\"\nHe never returned.",
"flags": {
"nsfw": false,
"religious": false,
"political": false,
"racist": false,
"sexist": false,
"explicit": false
},
"id": 18,
"safe": true,
"lang": "en"
}
 **/
