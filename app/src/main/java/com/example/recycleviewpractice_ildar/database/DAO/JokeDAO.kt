package com.example.recycleviewpractice_ildar.database.DAO

import androidx.room.*
import com.example.recycleviewpractice_ildar.database.models.Joke

@Dao
interface JokeDAO {

    @Query("SELECT * FROM Joke")
    fun getAllJokes(): MutableList<Joke>
    @Insert
    fun addJoke(jokeEntity: Joke)
    @Update
    fun updateJoke(jokeEntity: Joke)
    @Delete
    fun deleteJoke(jokeEntity: Joke)

}