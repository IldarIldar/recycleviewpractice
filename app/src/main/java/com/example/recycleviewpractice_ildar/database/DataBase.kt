package com.example.recycleviewpractice_ildar.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.recycleviewpractice_ildar.database.DAO.JokeDAO
import com.example.recycleviewpractice_ildar.database.models.Joke


@Database(entities = arrayOf(Joke::class), version = 1)
abstract class DataBase: RoomDatabase() {

    abstract fun JokeDao(): JokeDAO


}