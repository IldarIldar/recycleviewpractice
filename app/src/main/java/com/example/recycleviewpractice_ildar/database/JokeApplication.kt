package com.example.recycleviewpractice_ildar.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room

class JokeApplication: Application() {
    companion object {
        lateinit var database: DataBase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this, DataBase::class.java, "ContactDatabase").build()
    }
}
