package com.example.recycleviewpractice_ildar

import com.example.recycleviewpractice_ildar.database.models.Joke

interface OnClickListener {
   fun onClick(joke: Joke)


}