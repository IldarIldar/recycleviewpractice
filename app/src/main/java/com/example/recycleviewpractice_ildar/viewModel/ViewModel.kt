package com.example.recycleviewpractice_ildar.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.recycleviewpractice_ildar.API.InterfaceApi.Companion.createRetrofit
import com.example.recycleviewpractice_ildar.database.JokeApplication
import com.example.recycleviewpractice_ildar.database.models.Joke
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ViewModel: ViewModel() {


    val appJoke= JokeApplication
    val jokesModel= MutableLiveData<MutableList<Joke>>()
    val ogJokesIds= mutableSetOf<Int>() //comprobador de jokes nuevos
    var jokes:MutableList<Joke> = arrayListOf()


    fun deleteJoke(id:Int){
        jokes=getAllJokes()
        for (i in jokes.indices){
            if (jokes[i].id==id){

                ogJokesIds.remove(id)
                CoroutineScope(Dispatchers.IO).launch {
                    appJoke.database.JokeDao().deleteJoke(jokes[i]) //eliminamos de database
                    jokesModel.postValue(getAllJokes())
                }

                break
            }
        }
    }

    fun getAllJokes():MutableList<Joke>{

        val thread = CoroutineScope(Dispatchers.IO).launch {
            jokes = appJoke.database.JokeDao().getAllJokes()
        }
      while (!thread.isCompleted or thread.isCancelled) {
        }

        return jokes;
        }

    fun favJoke(joke: Joke, fav:Boolean){
        jokes=getAllJokes()

        for (jokee in jokes){
            if (jokee.id==joke.id){
                jokee.fav=fav
                CoroutineScope(Dispatchers.IO).launch {
                    appJoke.database.JokeDao().updateJoke(jokee)//updateJoke
                    jokesModel.postValue(getAllJokes())

                }
            }
        }
    }


    fun searchJoke(word:String):MutableList<Joke>{
        var search_jokes:MutableList<Joke> = arrayListOf()

        for (jokee in jokes){
            if (jokee.id.toString().length>=word.length && jokee.id.toString().subSequence(0,word.length).equals(word)){
                search_jokes.add(jokee)
            }
        }

        return search_jokes;
    }

     fun searchJoke(){

        CoroutineScope(Dispatchers.IO).launch {
            var contador=0
            var endLoop=false
            val call= createRetrofit().getData("https://v2.jokeapi.dev/joke/Programming?type=single")
            while (!endLoop){
                println("EN BUSCA DE CHISTES iteracion NUM "+contador)
                contador++

                val joke=call.body()
                if(call.isSuccessful) {
                    println("joke sucseful-->" + joke.toString())

                    if (joke != null) {
                        jokes=getAllJokes()//actualizamos jokes

                        if (!ogJokesIds.contains(joke.id)) {
                            ogJokesIds.add(joke.id) //nuevo id original
                            appJoke.database.JokeDao().addJoke(joke!!) //añadimos joke a database
                            //getAllJokes().forEach { joke -> println("JOKES AHORA num:" + joke.id) }
                            jokesModel.postValue(getAllJokes()) //new mutable live data
                            endLoop = true

                        }else if(contador>20){
                            endLoop=true
                            println("NO HAY MAS CHISTES DE PROGRAMMACION")
                        }
                    }
                }else{
                    println("ERROR en searchJoke viewModel")
                }

            }
        }

    }

    fun getFavorites():MutableList<Joke>{
        var favJokes:MutableList<Joke> = arrayListOf()

        var completeJokeList= getAllJokes();
        for (joke in completeJokeList){

            if(joke.fav){
                favJokes.add(joke)
            }
        }
        return favJokes;
    }

/**
    fun initJokes() {
        jokes={};
    }**/


    /**
//SETS
val vocalesRepetidas=setOf('a','e','y','o','u','e','i') //las repetidas desapareceran

mutableSetOf() //mutable

.add(elemento)
.remove(elemento) //no puedes poner posicion o key, solo el value
 **/

}


/**
if(jokes.isNullOrEmpty()){
var joke=Joke(false,name,0 )
jokes.add(joke)
jokesModel.postValue(jokes)
}else{
var newId=jokes.last().id+1
var joke=Joke(false,name,newId )
jokes.add(joke)
jokesModel.postValue(jokes)
}
jokes.forEach{joke -> println("-->"+joke.id) }    **/